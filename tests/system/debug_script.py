from arsenai.cli.arsenai import main


if __name__ == "__main__":
    main(
        [
            "-vv",
            "generate",
            "/home/stephan/data/work/palaestrai/"
            "arsenai/tests/fixtures/example_experiment.yml",
        ]
    )
